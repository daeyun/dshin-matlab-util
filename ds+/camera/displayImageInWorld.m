function displayImageInWorld(camera, im, scale)

[h, w, depth] = size(im);
imageCoords = scale*[0 0 1; 0 h 1; w 0 1; w h 1];
worldCoords = imageToWorld(camera, imageCoords);

im = imresize(im, 0.15);
[h, w, depth] = size(im);
[x, y, z] = meshgrid(1:w, 1:h, 1:depth);

surface(reshape(worldCoords(:, 1), 2, 2), reshape(worldCoords(:, 2), 2, 2),reshape(worldCoords(:, 3), 2, 2), 'FaceColor','texturemap', 'CData', im);
