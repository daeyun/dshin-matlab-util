function x = cameraFixationCentroid(cameras, images)

fixationLines = [];

for i = 1:numel(cameras)

R = cameras(i).R;
t = cameras(i).t;
K = cameras(i).K;

camPos = (-R'*t)';

[h, w, ~] = size(images{i});

%points = imageToWorld(cameras(i), [w/2 h/2 1]);
points = imageToWorld(cameras(i), [K(1,3) K(2,3) 1]);
%points = [R t; 0 0 0 1]\[0 0 1 1]';,  points=points(1:3,:);

fixationLines = [fixationLines; camPos points(:)'];

end

fixationLines(:, 4:6) = fixationLines(:, 4:6) - fixationLines(:, 1:3);

x = findLineIntersectionCentroid(fixationLines);
