function imageCoords = worldToImage(camera, worldCoords)
% imageCoords: N by 3  or  N by 2
% output: N by 2

R = camera.R;
t = camera.t;
K = camera.K;

if size(worldCoords, 2) == 3 && size(worldCoords, 1) ~= 3
    worldCoords = worldCoords';
end

imageCoords = K*[R t]*[worldCoords; ones(1, size(worldCoords, 2))];

imageCoords = imageCoords(1:2,:)./repmat(imageCoords(3,:), [2, 1]);
imageCoords = imageCoords';
