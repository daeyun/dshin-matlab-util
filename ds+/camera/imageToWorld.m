function worldCoords = imageToWorld(camera, imageCoords, scale)
% imageCoords: N by 3  or  N by 2
% output: N by 3

if size(imageCoords, 2) == 2
    imageCoords = [imageCoords ones(size(imageCoords, 1), 1)];
end

R = camera.R;
t = camera.t;
K = camera.K;

%%

if nargin >= 3
    imageCoords = imageCoords*scale;
end

cameraCoords = K\imageCoords';
worldCoords = camera.R'*(cameraCoords-repmat(camera.t, [1 size(cameraCoords, 2)]));
worldCoords = worldCoords';

%worldCoords = [P; 0 0 0 1]\[worldCoords; ones(1, size(worldCoords, 2))];
%worldCoords = worldCoords(1:3,:)';
