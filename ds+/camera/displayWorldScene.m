function displayWorldScene(cameras, images, voxelPts)

scale = 0.1;


camPostions = [];
camViewEdges = [];

setfig;

for i = 1:numel(cameras)

    R = cameras(i).R;
    t = cameras(i).t;
    K = cameras(i).K;

    %camPos = imageToWorld(cameras(i), [0 0 0]);
    camPos = (-R'*t)';
    camPostions = [camPostions; camPos];

    [h, w, depth] = size(images{i});

    imageCoords = scale * [0 0 1; w 0 1; w h 1; 0 h 1];

    worldCoords = imageToWorld(cameras(i), imageCoords);

    camViewEdges = [camViewEdges; [repmat(camPos, [4 1]) worldCoords]];

    displayImageInWorld(cameras(i), images{i}, scale);

end
set(gca, 'DataAspectRatio', [1 1 1]);

drawPoint3d(camPostions, 'marker', '.');
drawEdge3d(camViewEdges);

cameratoolbar('SetCoordSys', 'y');
axis image;


x = cameraFixationCentroid(cameras, images);
drawPoint3d(x, 'marker', '+', 'color', 'r');


if nargin > 2
    drawPoint3d(voxelPts);
end
