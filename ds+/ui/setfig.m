function setfig(v)
try varargin{:};, catch, varargin={}; end, p=inputParser;
addOptional(p, 'up', 'y');
addOptional(p, 'view', [1 1]);
parse(p, varargin{:}); args = p.Results;


hold on;
axis equal;
grid on;

if nargin < 2
    v = [1 1 1];
end

% axis([-1 1 -1 1 -1 1]);
set(gcf, 'renderer', 'opengl');
% set(gca, 'CameraPosition', [1000 4000 -6000]);
view(args.view(1), args.view(2));

cameratoolbar('Show');
cameratoolbar('SetMode', 'orbit');
cameratoolbar('SetCoordSys', 'y');

xlabel('x');
ylabel('y');
zlabel('z');

set(gcf,'KeyPressFcn', @clickCallback);

function clickCallback(fig_obj, ~)
    if get(fig_obj, 'CurrentCharacter')==' '
        campos
        [az,el] = view
    end
end

end
