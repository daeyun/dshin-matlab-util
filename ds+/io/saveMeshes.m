function saveMeshes(dirname)
%%


files = dir(dirname)
%%
for i = 1:numel(files)
    i
    file = files(i);
    if file.isdir
        continue
    end
    name = file.name;
    setInd = str2num(name(1));
    itemInd = str2num(name(3:5));

    meshset = matfile([dirname '/' name]);
    info = whos(meshset);
    for j = 1:numel(info)
        j
        mesh = meshset.(info(j).name);

        c1.camera = reconstructed_meshes_TestSubset{setInd}{itemInd}.camera_match;
        [M_match,P_match,V_match]=getCamera(c1);

        if j > 2
        mesh.v = M_match(1:3,1:3)*mesh.v;
        mesh.v([1 2],:) = mesh.v([2 1],:);
        mesh.v = (mesh.v*4/200)-2;
        mesh.v = M_match(1:3,1:3)\mesh.v;
        end

        h = figure('Visible','Off');
        displayMesh(mesh);
        mkdir(['~/hdd/reconImg/' sprintf('%04d', i-2)]);
        saveTightFigure(h, ['~/hdd/reconImg/' sprintf('%04d/%d', i-2, j) '.png']);
        close(h);
    end
end

%%

dirname = '~/hdd/Shape2014/DataOut/reconVoxel';
%%


files = dir(dirname);
%%
%for i = 1:numel(files)
for i = 1:3
    file = files(i);
    if file.isdir
        continue
    end
    name = file.name;

    voxeldata = matfile([dirname '/' name]);
    voxel = voxeldata.voxel(1,1);

    voxel_wsym = voxel{1};


    c1.camera = reconstructed_meshes_TestSubset{1}{1}.camera_match;
    [M_match,P_match,V_match]=getCamera(c1);
    

    [X,Y,Z,c] = voxelToIso(voxel_wsym == 1);
    [F,V] = MarchingCubes(X,Y,Z,c,0);
    if(numel(F)/3 > 10000)
    m = reducepatch(struct('faces', F, 'vertices', V), 10000/(numel(F)/3));
    mesh.f = m.faces';
    mesh.v = m.vertices';
    else
    mesh.f = F';
    mesh.v = V';
    end
    %mesh.v = (mesh.v*4/200)-2;

    mesh.v = M_match(1:3,1:3)\mesh.v;
    figure; setfig; displayMesh(mesh);


    depth = 7;
    [mesh2] = poissonSurfaceFromVoxel(voxel_wsym, ...
        'depth', depth, 'showFigure', false, 'includeRawSurface', true);

    mesh2.v = mesh2.v';
    mesh2.f = mesh2.f';
    mesh2.v([1 2],:) = mesh2.v([2 1],:);

    mesh2.v = M_match(1:3,1:3)\mesh2.v;
    figure; setfig; displayMesh(mesh2);



end



